import ApplicationAdapter from './application';

export default ApplicationAdapter.extend({
  host: 'https://www.reddit.com',
  namespace: 'r',
  urlForQuery(query, modelName) {
    let path = `${query.subreddit}.json`;
    delete query.subreddit;
    return `${this.host}/${this.namespace}/${path}`;
  }
});
