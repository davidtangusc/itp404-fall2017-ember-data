import ApplicationSerializer from './application';
import Ember from 'ember';

export default ApplicationSerializer.extend({
  primaryKey: 'login',
  keyForAttribute(key, method) {
    return Ember.String.underscore(key);
  }
});
