import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  normalizeQueryResponse(store, primaryModelClass, payload, id, requestType) {
    let posts = payload.data.children.map((item) => {
      return item.data;
    });

    return this._super(store, primaryModelClass, posts, id, requestType);
  }
});
